local places = {
    "7, 16",
    "Zergonipal, Shrine",
    "Ancient Shipwreck, Upper Level",
    "Ald-ruhn, Council Hostel",
    "Caldera, Shenk's Shovel",
    "-9, 17",
    "Mannammu",
    "Gnisis, Temple",
    "-14, 15",
    "Balmora, Temple",
    "Ebonheart, Imperial Chapels",
    "Fair Helas, Cabin",
    "Gnisis, Temple",
    "-4, 18",
    "Yanemus Mine",
    "Sadryon Ancestral Tomb",
    "Samarys Ancestral Tomb",
    "Seyda Neen, Arrille's Tradehouse",
    "Maar Gan, Shrine",
    "Bal Ur, Shrine",
    "-8, 3",
    "-6, -5",
    "Ibar-Dad",
    "Ashmelech"
}

local function teleport_to_statue(pid)
    raise.player.teleport(pid, "Interworld", {
            posX = 1278.101562,
            posY = 6455.671387,
            posZ = 346.942078
        },
        {
            rotX = -0.22730131449955,
            rotZ = 3.115789811
        })
end

local function random_place(pid)
    mpWidgets.Notification(pid, "Да будет так.")
    logicHandler.RunConsoleCommandOnPlayer(pid, "fadeout 2", false)
    
    timerApiEx.CreateTimer("ressurect_random_" .. tostring(pid), time.seconds(2), function()
        local rnd = luastd.Random.hw(1, #places)
        local place = places[rnd]
        raise.player.teleport(pid, place)
    end)
    timerApiEx.StartTimer("ressurect_random_" .. tostring(pid))
end

local function statue_menu(pid)
    local id = mpWidgets.New("Твои молитвы услашаны, смертный! Ты можешь вернуться в свой мир несколькими способами. Выбирай мудро.")
    mpWidgets.AddButton(id, "Использовать монету", nil)
    mpWidgets.AddButton(id, "Поворот судьбы", random_place)
    mpWidgets.AddButton(id, "Жертва здоровьем", nil)
    mpWidgets.AddButton(id, "Жертва временем", nil)
    mpWidgets.AddButton(id, "Я передумал", function(pid)
        mpWidgets.Notification(pid, "Не трать свое время, смертный. У тебя его слишком мало.")
    end)
    mpWidgets.DeleteLater(id)
    mpWidgets.Show(pid, id, mpWidgets.Type.MessageBox)
end

local function chargen_guardtalk(pid)
    -- Подойдите к следующему зданию и поговорите с Селусом Гравиусом.
    tes3mp.PlaySpeech(pid, "vo\\Misc\\CharGen Door2.wav")
    tes3mp.PlaySpeech(pid, "Fx\\trans\\chain_pul2.wav")
end

local function open_chargendoor(eventStatus, pid, cell, objects, players)
    if cell == "Interworld" then
        if objects[1].refId == "nech_azura_statue2" then
            statue_menu(pid)
            return customEventHooks.makeEventStatus(false,false)
        end
               
        if tableHelper.containsValue(LoadedCells[cell].data.packets.actorList, objects[1].uniqueIndex, false) then
            return customEventHooks.makeEventStatus(true,true)
        else
            return customEventHooks.makeEventStatus(false,false)
        end
    end
end

customCommandHooks.registerCommand("fixme", function(pid)
    if tes3mp.GetCell(pid) == "Interworld" then
        teleport_to_statue(pid)
    else
        unstack.fixme(pid)
    end
end)

customCommandHooks.registerCommand("suicide", function(pid)
    if tes3mp.GetCell(pid) == "Interworld" then
        mpWidgets.Notification(pid, "Здесь нет смерти.")
    else
        tes3mp.SetHealthCurrent(pid, 0)
        tes3mp.SendStatsDynamic(pid)
    end
end)

--customEventHooks.registerValidator("OnObjectActivate", open_chargendoor)

customEventHooks.registerHandler("OnPlayerDeath", function(eventStatus, pid)
    logicHandler.RunConsoleCommandOnPlayer(pid, "startscript FL_FixFuckingMPBug", false)
end)
customEventHooks.registerValidator("OnDeathTimeExpiration", function(eventStatus, pid)
    tes3mp.Resurrect(pid, enumerations.resurrect.REGULAR)
    return customEventHooks.makeEventStatus(false,false)
end)
